#!/bin/sh
#
#  Customise the distribution post-install.
#

# NOTE: If running rinse multiple times with same target dir, this script builds on already done
# installation. It can cause subtle differences compared to a fresh installation.

echo "Running post install customizations..."

prefix=$1

if [ ! -d "${prefix}" ]; then
    echo "Serious error - the named directory doesn't exist."
    exit
fi

#
#  2.  Copy the cached .RPM files into the zypper directory, so that
#     zypper doesn't need to fetch them again.
#

if [ "${ARCH}" = "amd64" ]; then
    REAL_ARCH="x86_64"
else
    REAL_ARCH="i586"
fi

mkdir -p ${prefix}/var/cache/zypp/packages/opensuse/noarch
mkdir -p ${prefix}/var/cache/zypp/packages/opensuse/${REAL_ARCH}
#cp -p ${cache_dir}/${dist}.${ARCH}/* ${prefix}/var/cache/zypp/packages/opensuse/suse/${ARCH}
cp -r ${cache_dir}/${dist}.${ARCH}/*.noarch.rpm ${prefix}/var/cache/zypp/packages/opensuse/noarch
cp -r ${cache_dir}/${dist}.${ARCH}/*.${REAL_ARCH}.rpm ${prefix}/var/cache/zypp/packages/opensuse/${REAL_ARCH}

#
#  3.  Reinstall everything in order to get scriptlets (e.g. %post ) run correctly.
#

# NOTE: These can be used to bootstrap the environment in case zypper refuses to work. Zypper is probably missing some dependency,
# but this can aid with debugging. When zypper works, these probably causes different end result from standard installation.

# Creates necessary base files.
#chroot ${prefix} /bin/bash -c 'rpm --reinstall /var/cache/zypp/packages/opensuse/suse/${ARCH}/openSUSE-release*.rpm'

# Then just run all post scriptlets.
# --nodeps is necessary since the environment is not yet complete.
#chroot ${prefix} /bin/bash -c 'for i in /var/cache/zypp/packages/opensuse/suse/${ARCH}/* ; do rpm --nodeps --reinstall $i ; done'

#
#  4.  Ensure that zypper has a working configuration file.
#

arch=i386
if [ $ARCH = "amd64" ] ; then
    arch=x86_64
fi

[ -d "${prefix}/etc/zypp/repos.d" ] || mkdir -p ${prefix}/etc/zypp/repos.d

# gpgcheck is failing here, since system is not complete.
cat > ${prefix}/etc/zypp/repos.d/${dist}.repo <<EOF
[opensuse]
name=${dist}
enabled=1
autorefresh=1
baseurl=$(dirname ${mirror})/oss/
path=/
type=rpm-md
gpgcheck=0
EOF

# TODO: openSUSE is still speaking about i586... Does that matter here?

if [ $ARCH = "i386" ] ; then
    echo "  Setting architecture to i686"
    sed -i 's/\(# \)\?arch = .*/arch = i686/' ${prefix}/etc/zypp/zypp.conf
fi

#
#  5.  Run "zypper install zypper".
#

echo "  Bootstrapping zypper"

# TODO: why this kind of passwd file...?

# No /etc/passwd at this point
cat > ${prefix}/etc/passwd <<EOT
root:x:0:0:root:/root:/bin/bash
lp:x:4:7:Printing daemon:/var/spool/lpd:/bin/bash
mail:x:8:12:Mailer daemon:/var/spool/clientmqueue:/bin/false
news:x:9:13:News system:/etc/news:/bin/bash
uucp:x:10:14:Unix-to-Unix CoPy system:/etc/uucp:/bin/bash
man:x:13:62:Manual pages viewer:/var/cache/man:/bin/bash
nobody:x:65534:65533:nobody:/var/lib/nobody:/bin/bash
wwwrun:x:30:8:WWW daemon apache:/var/lib/wwwrun:/bin/false
EOT

# TODO: Ditto with passwd

cat > ${prefix}/etc/group <<EOT
root:x:0:
tty:x:5:
lp:x:7:
mail:x:12:
news:x:13:
uucp:x:14:
shadow:x:15:
dialout:x:16:
lock:x:54:
EOT

# TODO TODO
#exit

# Need key trusted to prevent warnings during package install
chroot ${prefix} /usr/bin/zypper -n refresh --force

# Force openSUSE-release to be reinstalled so that the config is correct.
chroot ${prefix} /usr/bin/zypper -n install openSUSE-release
# Second installation is necessary.
chroot ${prefix} /usr/bin/zypper -n install -f openSUSE-release

# The base system
chroot ${prefix} /usr/bin/zypper -n install aaa_base vim 2>&1
chroot ${prefix} /usr/bin/zypper -n update              2>&1
chroot ${prefix} /usr/bin/zypper clean

#
#  6.  Clean up
#
umount ${prefix}/proc
umount ${prefix}/sys

# Packages in root and Zypper’s cache directories
rm -f  ${prefix}/*.rpm ${prefix}/var/cache/zypp/packages/opensuse/suse/noarch/*.rpm ${prefix}/var/cache/zypp/packages/opensuse/suse/${REAL_ARCH}/*.rpm

# TODO: these sound strange... Especially latter one shouldn’t happen in any case.
find ${prefix} -name '*.rpmorig' -delete
find ${prefix} -name '*.rpmnew' -delete

#
#  7.  Post hook changes.
#

chroot ${prefix} sed -i 's/gpgcheck=0/gpgcheck=1/' /etc/zypp/repos.d/${dist}.repo
