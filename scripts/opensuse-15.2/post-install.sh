#!/bin/sh
#
#  Customise the distribution post-install.
#

# NOTE: If running rinse multiple times with same target dir, this script builds on already done
# installation. It can cause subtle differences compared to a fresh installation.

echo "Running post install customizations..."

prefix=$1

if [ ! -d "${prefix}" ]; then
    echo "Serious error - the named directory doesn't exist."
    exit
fi

# Have resolv.conf for domain operations
# TODO: have means for fully offline operation.
cp -L /etc/resolv.conf ${prefix}/etc/resolv.conf

#
#  2.  Copy the cached .RPM files into the zypper directory, so that
#     zypper doesn't need to fetch them again.
#

if [ "${ARCH}" = "amd64" ]; then
    REAL_ARCH="x86_64"
else
    REAL_ARCH="i586"
fi

mkdir -p ${prefix}/var/cache/zypp/packages/opensuse/noarch
mkdir -p ${prefix}/var/cache/zypp/packages/opensuse/${REAL_ARCH}
#cp -p ${cache_dir}/${dist}.${ARCH}/* ${prefix}/var/cache/zypp/packages/opensuse/suse/${ARCH}
cp -r ${cache_dir}/${dist}.${ARCH}/*.noarch.rpm ${prefix}/var/cache/zypp/packages/opensuse/noarch
cp -r ${cache_dir}/${dist}.${ARCH}/*.${REAL_ARCH}.rpm ${prefix}/var/cache/zypp/packages/opensuse/${REAL_ARCH}

#
#  3.  Reinstall everything in order to get scriptlets (e.g. %post ) run correctly.
#

# NOTE: These can be used to bootstrap the environment in case zypper refuses to work. Zypper is probably missing some dependency,
# but this can aid with debugging. When zypper works, these probably causes different end result from standard installation.

# Creates necessary base files.
#chroot ${prefix} /bin/bash -c 'rpm --reinstall /var/cache/zypp/packages/opensuse/suse/${ARCH}/openSUSE-release*.rpm'

# Then just run all post scriptlets.
# --nodeps is necessary since the environment is not yet complete.
#chroot ${prefix} /bin/bash -c 'for i in /var/cache/zypp/packages/opensuse/suse/${ARCH}/* ; do rpm --nodeps --reinstall $i ; done'

#
#  4.  Ensure that zypper has a working configuration file.
#

arch=i386
if [ $ARCH = "amd64" ] ; then
    arch=x86_64
fi

[ -d "${prefix}/etc/zypp/repos.d" ] || mkdir -p ${prefix}/etc/zypp/repos.d

# gpgcheck is failing here, since system is not complete.
cat > ${prefix}/etc/zypp/repos.d/${dist}.repo <<EOF
[opensuse]
name=${dist}
enabled=1
autorefresh=1
baseurl=$(dirname ${mirror})/oss/
path=/
type=rpm-md
gpgcheck=0
EOF

# TODO: openSUSE is still speaking about i586... Does that matter here?

if [ $ARCH = "i386" ] ; then
    echo "  Setting architecture to i686"
    sed -i 's/\(# \)\?arch = .*/arch = i686/' ${prefix}/etc/zypp/zypp.conf
fi

#
#  5.  Run "zypper install zypper".
#

echo "  Bootstrapping zypper"

zypper_command="chroot ${prefix} /usr/bin/zypper"
zypper_install="chroot ${prefix} /usr/bin/zypper --non-interactive install --no-recommends"

# Need repo key trusted to prevent warnings during package install.
# TODO: For security, it should be possible to specify location of the key that is trusted.
${zypper_command} --gpg-auto-import-keys -n refresh --force

# Force openSUSE-release to be reinstalled so that the config is correct.
${zypper_install} openSUSE-release
# Second installation is necessary.
${zypper_install} -f openSUSE-release

#
# 6. Reconstruct base system
#

# cracklib-dict-small needs to be specified so that full version does not get installed here.
${zypper_install} aaa_base vim cracklib-dict-small 2>&1
${zypper_command} --non-interactive update --no-recommends 2>&1

# This is what OpenSUSE thinks to be a minimal system.
${zypper_install} -t pattern minimal_base

# Then just install all packages rinse decompressed.
chroot "${prefix}" rpm -Uvh /var/cache/zypp/packages/opensuse/*/*.rpm

#
#  7.  Clean up
#

${zypper_command} clean

umount ${prefix}/proc
umount ${prefix}/sys

# Remove packages copied to installation’s root.
rm -f  ${prefix}/*.rpm

#
#  7.  Post hook changes.
#

chroot ${prefix} sed -i 's/gpgcheck=0/gpgcheck=1/' /etc/zypp/repos.d/${dist}.repo
